from os import system, name
import numpy as np

from enum import Enum
from cell import Cell, CellState
from index import Index


class Direction(Enum):
    UP = 0
    DOWN = 1
    LEFT = 2
    RIGHT = 3


class Board(object):
    def __init__(self, initial_state):
        self.__shape = Index((initial_state["shape"]["row"], initial_state["shape"]["col"]))
        self.__board_state = np.empty(shape=self.__shape.as_tuple, dtype=np.object_)
        indices = []
        for index in initial_state['indices']:
            indices.append((index["row"], index["col"]))

        for (row, col), value in np.ndenumerate(self.__board_state):
            if (row, col) in indices:
                self.__board_state[row][col] = Cell(CellState.ALIVE, (row, col))
            else:
                self.__board_state[row][col] = Cell(CellState.DEAD, (row, col))

    def next(self):
        self.__calculate()
        self.__apply()

    def __calculate(self):
        for _, cell in np.ndenumerate(self.__board_state):
            sum_of_neighbors = self.__sum_of_neighbors(cell)
            if cell.alive and 2 > sum_of_neighbors or sum_of_neighbors > 3:
                cell.kill()
            elif cell.dead and sum_of_neighbors == 3:
                cell.revive()

    def __apply(self):
        for _, cell in np.ndenumerate(self.__board_state):
            cell.apply()

    def __valid_cell(self, cell):
        return 0 <= cell.index.col < self.__shape.col and 0 <= cell.index.row < self.__shape.row

    def __sum_of_neighbors(self, cell: Cell):
        neighbors = []
        cell_index = cell.index
        allowed_directions = []

        if cell_index.row-1 > 0:
            allowed_directions.append(Direction.LEFT)
        if cell_index.row+1 < self.__shape.row:
            allowed_directions.append(Direction.RIGHT)
        if cell_index.col-1 > 0:
            allowed_directions.append(Direction.UP)
        if cell_index.col+1 < self.__shape.col:
            allowed_directions.append(Direction.DOWN)

        if Direction.UP in allowed_directions:
            neighbors.append(self.__board_state[cell_index.row, cell_index.col - 1])
            if Direction.RIGHT in allowed_directions:
                neighbors.append(self.__board_state[cell_index.row + 1, cell_index.col - 1])
            if Direction.LEFT in allowed_directions:
                neighbors.append(self.__board_state[cell_index.row - 1, cell_index.col - 1])

        if Direction.DOWN in allowed_directions:
            neighbors.append(self.__board_state[cell_index.row, cell_index.col + 1])
            if Direction.RIGHT in allowed_directions:
                neighbors.append(self.__board_state[cell_index.row + 1, cell_index.col + 1])
            if Direction.LEFT in allowed_directions:
                neighbors.append(self.__board_state[cell_index.row - 1, cell_index.col + 1])

        if Direction.RIGHT in allowed_directions:
            neighbors.append(self.__board_state[cell_index.row + 1, cell_index.col])

        if Direction.LEFT in allowed_directions:
            neighbors.append(self.__board_state[cell_index.row - 1, cell_index.col])

        return Board.__sum_neighbors(neighbors)

    @staticmethod
    def __sum_neighbors(neighbors):
        sum_of_neighbors = 0
        for cell in neighbors:
            if cell.alive:
                sum_of_neighbors += 1

        return sum_of_neighbors

    @staticmethod
    def __clear_screen():
        # for windows
        if name == 'nt':
            _ = system('cls')

        # for mac and linux(here, os.name is 'posix')
        else:
            _ = system('clear')

    def print(self):
        Board.__clear_screen()
        self.__print_line()

        string_to_print = f"|"
        for (row, col), cell in np.ndenumerate(self.__board_state):
            if row != 0 and col == 0:
                string_to_print = f"{string_to_print}\n|"

            if cell.alive:
                string_to_print = f"{string_to_print}*"
            else:
                string_to_print = f"{string_to_print} "

            if col == self.__shape.col - 1:
                string_to_print = f"{string_to_print}|"

        print(string_to_print)

        self.__print_line()

    def __print_line(self):
        string_to_print = " "
        for _ in range(0, self.__shape.col):
            string_to_print = f"{string_to_print}-"
        string_to_print = f"{string_to_print} "

        print(string_to_print)


