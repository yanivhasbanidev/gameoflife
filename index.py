

class Index(object):
    def __init__(self, raw_index):
        self.__row = raw_index[0]
        self.__col = raw_index[1]

    @property
    def row(self):
        return self.__row

    @property
    def col(self):
        return self.__col

    @property
    def as_tuple(self):
        return [self.__row, self.__col]