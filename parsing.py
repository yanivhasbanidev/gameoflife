import json


class ParserException(Exception):
    pass


class Parser(object):
    @staticmethod
    def parse(input_file_path):
        with open(input_file_path, "r") as input_file:
            data = input_file.read()

        try:
            loaded_json = json.loads(data)
        except json.JSONDecodeError as e:
            raise ParserException(f"Failed to parse json. error = {e}")
        except Exception as unknown_error:
            raise ParserException(f"Unknown json parsing exception = {unknown_error}")

        if "shape" not in loaded_json or "indices" not in loaded_json:
            raise ParserException("Keys \"shape\" or \"indices\" do not exist in json")

        return loaded_json
