from enum import Enum
from index import Index

class CellState(Enum):
    DEAD = 0
    ALIVE = 1
    UNINITIALIZED = 2


class Cell(object):
    def __init__(self, initial_state: CellState, index):
        self.__state = initial_state
        self.__next_state = initial_state
        self.__index = Index(index)

    @property
    def index(self):
        return self.__index

    @property
    def alive(self):
        return self.__state == CellState.ALIVE

    @property
    def dead(self):
        return self.__state == CellState.DEAD

    def revive(self):
        self.__next_state = CellState.ALIVE

    def kill(self):
        self.__next_state = CellState.DEAD

    def apply(self):
        self.__state = self.__next_state
