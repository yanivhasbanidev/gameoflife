from time import sleep

import click

from threading import Timer
from board import Board
from parsing import Parser


class GameOfLife(object):
    def __init__(self, input_json, number_of_generation):
        self.__board = Board(input_json)
        self.__thread = Timer(0.1, self.update)
        self.__number_of_generation = number_of_generation
        self.__current_number_of_generations = 0

    def update(self):
        while self.__current_number_of_generations <= self.__number_of_generation:
            self.__print_board()
            self.__board.next()
            self.__current_number_of_generations += 1
            sleep(0.1)

    def start(self):
        self.__thread.start()
        self.__thread.join()

    def __print_board(self):
        self.__board.print()


@click.command()
@click.argument("initial_state_file_path", default="/Users/yanivhasbani/Developer/mt/initial_state.json")
@click.argument("number_of_generation", default=10)
def start(initial_state_file_path, number_of_generation):
    parsed_input = Parser.parse(initial_state_file_path)
    game = GameOfLife(parsed_input, number_of_generation)
    game.start()


if __name__ == "__main__":
    start()

